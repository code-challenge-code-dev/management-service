package com.management.domain.services;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.services.useraccount.UpdateUserAccountService;
import com.management.kafka.dtos.SendNotificationMessage;
import com.management.kafka.producer.NotificationToSendTopicProducerService;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UpdateUserAccountServiceTest {

    @Mock
    private UserAccountRepository userAccountRepository;

    @Mock
    private NotificationToSendTopicProducerService sendCommunicationTopicProducerService;

    @InjectMocks
    private UpdateUserAccountService updateUserAccountService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testExecute() {
        UUID userId = UUID.randomUUID();
        UserAccount userAccount = new UserAccount();
        userAccount.setId(userId);
        userAccount.setEmail("matheus.updated@email.com");
        userAccount.setName("Matheus Updated Name");

        when(userAccountRepository.save(userAccount)).thenReturn(userAccount);

        UserAccount updatedUserAccount = updateUserAccountService.execute(userAccount);

        verify(userAccountRepository, times(1)).save(userAccount);

        ArgumentCaptor<SendNotificationMessage> messageCaptor = ArgumentCaptor.forClass(SendNotificationMessage.class);
        verify(sendCommunicationTopicProducerService, times(1)).execute(messageCaptor.capture());

        assertEquals(userAccount, updatedUserAccount);
    }
}