package com.management.domain.services;

import com.management.domain.entities.Department;
import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.services.useraccount.CreateUserAccountService;
import com.management.domain.usecases.kafka.SendNotificationTopicProducerUseCase;
import com.management.kafka.dtos.SendNotificationMessage;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CreateUserAccountServiceTest {

    @Mock
    private UserAccountRepository userAccountRepositoryMock;

    @Mock
    private SendNotificationTopicProducerUseCase sendNotificationTopicProducerUseCaseMock;

    @InjectMocks
    private CreateUserAccountService createUserAccountService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testExecute() {
        Department department = new Department(
            UUID.randomUUID(),
            "Department Name",
            null
        );
        UserAccount userAccount = new UserAccount(
            UUID.randomUUID(),
            "Matheus",
            "matheus@email.com",
            department
        );
        UserAccount savedUserAccount = new UserAccount(
            UUID.randomUUID(),
            "Matheus",
            "matheus@email.com",
            department
        );

        when(userAccountRepositoryMock.save(any(UserAccount.class))).thenReturn(savedUserAccount);

        UserAccount result = createUserAccountService.execute(userAccount);

        verify(userAccountRepositoryMock, times(1)).save(userAccount);

        ArgumentCaptor<SendNotificationMessage> captor = ArgumentCaptor.forClass(SendNotificationMessage.class);
        verify(sendNotificationTopicProducerUseCaseMock, times(1)).execute(captor.capture());

        assertEquals(savedUserAccount, result);
    }
}