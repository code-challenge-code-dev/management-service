package com.management.domain.services;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.services.useraccount.FindAllUserAccountService;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class FindAllUserAccountServiceTest {

    @Mock
    private UserAccountRepository userAccountRepositoryMock;

    @InjectMocks
    private FindAllUserAccountService findAllUserAccountService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testExecute() {
        UserAccount user1 = new UserAccount();
        user1.setId(UUID.randomUUID());
        user1.setName("Matheus");
        user1.setEmail("matheus@email.com");

        UserAccount user2 = new UserAccount();
        user2.setId(UUID.randomUUID());
        user2.setName("Matheus 2");
        user2.setEmail("matheus2@email.com");

        List<UserAccount> expectedUsers = Arrays.asList(user1, user2);

        when(userAccountRepositoryMock.findAll()).thenReturn(expectedUsers);

        List<UserAccount> actualUsers = findAllUserAccountService.execute();

        verify(userAccountRepositoryMock, times(1)).findAll();

        assertEquals(expectedUsers, actualUsers);
    }
}