package com.management.domain.services;

import com.management.domain.entities.Department;
import com.management.domain.repository.DepartmentRepository;
import com.management.domain.services.department.FindAllDepartmentService;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class FindAllDepartmentServiceTest {

    @Mock
    private DepartmentRepository departmentRepositoryMock;

    @InjectMocks
    private FindAllDepartmentService findAllDepartmentService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testExecute() {
        Department department1 = new Department();
        department1.setId(UUID.randomUUID());
        department1.setName("HR");

        Department department2 = new Department();
        department2.setId(UUID.randomUUID());
        department2.setName("IT");

        List<Department> expectedDepartments = Arrays.asList(department1, department2);

        when(departmentRepositoryMock.findAll()).thenReturn(expectedDepartments);

        List<Department> actualDepartments = findAllDepartmentService.execute();

        verify(departmentRepositoryMock, times(1)).findAll();

        assertEquals(expectedDepartments, actualDepartments);
    }
}
