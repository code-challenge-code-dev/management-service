package com.management.domain.services;

import com.management.domain.repository.UserAccountRepository;
import com.management.domain.services.useraccount.DeleteUserAccountService;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class DeleteUserAccountServiceTest {

    @Mock
    private UserAccountRepository userAccountRepositoryMock;

    @InjectMocks
    private DeleteUserAccountService deleteUserAccountService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testExecute() {
        UUID userAccountId = UUID.randomUUID();

        deleteUserAccountService.execute(userAccountId);

        verify(userAccountRepositoryMock, times(1)).deleteById(userAccountId);
    }
}
