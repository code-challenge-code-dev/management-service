package com.management.domain.mappers;

import com.management.domain.entities.Department;
import com.management.dtos.response.DepartmentResponse;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DepartmentMapperTest {

    private final DepartmentMapper departmentMapper = new DepartmentMapperImpl();

    @Test
    void testMapDepartmentToDepartmentResponse() {
        UUID departmentId = UUID.randomUUID();
        Department department = Department.builder()
            .id(departmentId)
            .name("IT")
            .build();

        DepartmentResponse departmentResponse = departmentMapper.mapDepartmentToDepartmentResponse(department);

        assertEquals(department.getId(), departmentResponse.id());
        assertEquals(department.getName(), departmentResponse.name());
    }
}
