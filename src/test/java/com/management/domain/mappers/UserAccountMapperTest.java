package com.management.domain.mappers;

import com.management.domain.entities.Department;
import com.management.domain.entities.UserAccount;
import com.management.dtos.request.CreateUserAccountRequest;
import com.management.dtos.response.UserAccountResponse;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserAccountMapperTest {

    private final UserAccountMapper userAccountMapper = new UserAccountMapperImpl();

    @Test
    void testMapUserAccountToUserAccountResponse() {
        UserAccount userAccount = UserAccount.builder()
            .id(UUID.randomUUID())
            .name("Matheus")
            .email("matheus@gmail.com")
            .department(
                Department.builder()
                    .id(UUID.randomUUID())
                    .name("IT")
                    .build()
            )
            .build();

        UserAccountResponse userAccountResponse = userAccountMapper.mapUserAccountToUserAccountResponse(userAccount);

        assertEquals(userAccount.getId(), userAccountResponse.id());
        assertEquals(userAccount.getName(), userAccountResponse.name());
        assertEquals(userAccount.getEmail(), userAccountResponse.email());
        assertEquals(userAccount.getDepartment().getName(), userAccountResponse.department().getName());
    }

    @Test
    void testMapUserAccountRequestToUserAccount() {
        CreateUserAccountRequest createUserAccountRequest = CreateUserAccountRequest.builder()
            .name("Matheus")
            .email("matheus@email.com")
            .department(
                Department.builder().id(
                    UUID.randomUUID())
                    .name("IT")
                    .build()
            )
            .build();

        UserAccount userAccount = userAccountMapper.mapUserAccountRequestToUserAccount(createUserAccountRequest);

        assertEquals(createUserAccountRequest.name(), userAccount.getName());
        assertEquals(createUserAccountRequest.email(), userAccount.getEmail());
        assertEquals(createUserAccountRequest.department().getId(), userAccount.getDepartment().getId());
    }

}