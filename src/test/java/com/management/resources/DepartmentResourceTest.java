package com.management.resources;

import com.management.domain.entities.Department;
import com.management.domain.usecases.department.FindAllDepartmentUseCase;
import com.management.dtos.response.DepartmentResponse;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class DepartmentResourceTest {

    @Mock
    private FindAllDepartmentUseCase findAllDepartmentUseCase;

    @InjectMocks
    private DepartmentResource departmentResource;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testFindByAll() {
        UUID departmentId1 = UUID.randomUUID();
        UUID departmentId2 = UUID.randomUUID();
        Department department1 = Department.builder()
            .id(departmentId1)
            .name("HR")
            .build();
        Department department2 = Department.builder()
            .id(departmentId2)
            .name("IT")
            .build();

        List<Department> departments = Arrays.asList(department1, department2);

        when(findAllDepartmentUseCase.execute()).thenReturn(departments);

        DepartmentResponse response1 = DepartmentResponse.builder()
            .id(departmentId1)
            .name("HR")
            .build();
        DepartmentResponse response2 = DepartmentResponse.builder()
            .id(departmentId2)
            .name("IT")
            .build();

        List<DepartmentResponse> expectedResponse = Arrays.asList(response1, response2);

        ResponseEntity<List<DepartmentResponse>> responseEntity = departmentResource.findByAll();

        assertEquals(200, responseEntity.getStatusCode().value());
        assertEquals(expectedResponse, responseEntity.getBody());
    }
}