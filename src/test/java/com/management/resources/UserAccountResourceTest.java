package com.management.resources;

import com.management.domain.entities.Department;
import com.management.domain.entities.UserAccount;
import com.management.domain.usecases.useraccount.CreateUserAccountUseCase;
import com.management.domain.usecases.useraccount.DeleteUserAccountUseCase;
import com.management.domain.usecases.useraccount.FindAllUserAccountUseCase;
import com.management.domain.usecases.useraccount.FindUserAccountByIdUseCase;
import com.management.domain.usecases.useraccount.UpdateUserAccountUseCase;
import com.management.dtos.request.CreateUserAccountRequest;
import com.management.dtos.response.UserAccountResponse;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UserAccountResourceTest {

    @Mock
    private FindAllUserAccountUseCase findAllUserAccountUseCaseMock;

    @Mock
    private FindUserAccountByIdUseCase findUserAccountByIdUseCaseMock;

    @Mock
    private CreateUserAccountUseCase createUserAccountUseCaseMock;

    @Mock
    private UpdateUserAccountUseCase updateUserAccountUseCaseMock;

    @Mock
    private DeleteUserAccountUseCase deleteUserAccountUseCaseMock;

    @InjectMocks
    private UserAccountResource userAccountResource;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testFindByAll() {
        Department department = Department.builder()
            .id(UUID.randomUUID())
            .name("Department Name")
            .build();

        UserAccount userAccount = UserAccount.builder()
            .id(UUID.randomUUID())
            .name("Teste")
            .email("teste@teste.com")
            .department(department)
            .build();

        when(findAllUserAccountUseCaseMock.execute()).thenReturn(Collections.singletonList(userAccount));

        ResponseEntity<List<UserAccountResponse>> responseEntity = userAccountResource.findByAll();

        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCode().value());

        List<UserAccountResponse> returnedUserAccounts = responseEntity.getBody();
        assert returnedUserAccounts != null;
        assertEquals(1, returnedUserAccounts.size());
        assertEquals("Teste", returnedUserAccounts.get(0).name());
        assertEquals("teste@teste.com", returnedUserAccounts.get(0).email());
    }

    @Test
    void testFindById() {
        Department department = Department.builder()
            .id(UUID.randomUUID())
            .name("Department Name")
            .build();

        UUID userId = UUID.randomUUID();
        UserAccount userAccount = UserAccount.builder()
            .id(userId)
            .name("Teste")
            .email("teste@teste.com")
            .department(department)
            .build();

        when(findUserAccountByIdUseCaseMock.execute(userId)).thenReturn(userAccount);

        ResponseEntity<UserAccountResponse> responseEntity = userAccountResource.findById(
            userId //,
            //Locale.ENGLISH
        );

        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCode().value());

        UserAccountResponse returnedUserAccount = responseEntity.getBody();
        assert returnedUserAccount != null;
        assertEquals("Teste", returnedUserAccount.name());
        assertEquals("teste@teste.com", returnedUserAccount.email());
    }

    @Test
    void testSave() {
        Department department = Department.builder()
            .id(UUID.randomUUID())
            .name("Department Name")
            .build();

        UUID userId = UUID.randomUUID();

        CreateUserAccountRequest createUserAccountRequest = CreateUserAccountRequest.builder()
            .id(userId)
            .name("Teste")
            .email("teste@teste.com")
            .department(department)
            .build();

        UserAccount userAccountSaved = UserAccount.builder()
            .id(userId)
            .name("Teste")
            .email("teste@teste.com")
            .department(department)
            .build();
        when(createUserAccountUseCaseMock.execute(any())).thenReturn(userAccountSaved);

        ResponseEntity<UserAccountResponse> responseEntity = userAccountResource.save(createUserAccountRequest);

        assertEquals(HttpStatus.CREATED.value(), responseEntity.getStatusCode().value());

        URI location = responseEntity.getHeaders().getLocation();
        assert location != null;
        assertEquals("/user-accounts/" + userAccountSaved.getId(), location.getPath());

        UserAccountResponse returnedUserAccount = responseEntity.getBody();
        assert returnedUserAccount != null;
        assertEquals("Teste", returnedUserAccount.name());
        assertEquals("teste@teste.com", returnedUserAccount.email());
    }

    @Test
    void testUpdate() {
        Department department = Department.builder()
            .id(UUID.randomUUID())
            .name("Department Name")
            .build();

        UUID userId = UUID.randomUUID();

        CreateUserAccountRequest updateUserAccountRequest = CreateUserAccountRequest.builder()
            .id(userId)
            .name("Teste Name")
            .email("testeupdated@teste.com")
            .department(department)
            .build();

        ResponseEntity<?> responseEntity = userAccountResource.update(userId, updateUserAccountRequest);

        assertEquals(HttpStatus.NO_CONTENT.value(), responseEntity.getStatusCode().value());

        verify(updateUserAccountUseCaseMock, times(1)).execute(any());
    }

    @Test
    void testDelete() {
        UUID userId = UUID.randomUUID();

        ResponseEntity<?> responseEntity = userAccountResource.delete(userId);

        assertEquals(HttpStatus.NO_CONTENT.value(), responseEntity.getStatusCode().value());

        verify(deleteUserAccountUseCaseMock, times(1)).execute(userId);
    }

}