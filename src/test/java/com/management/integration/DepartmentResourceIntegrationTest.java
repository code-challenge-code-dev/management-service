package com.management.integration;

import com.management.domain.entities.Department;
import java.util.UUID;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DepartmentResourceIntegrationTest extends AbstractIntegrationTestConfiguration {

    private final MockMvc mockMvc;

    @Autowired
    public DepartmentResourceIntegrationTest(
        MockMvc mockMvc
    ) {
        this.mockMvc = mockMvc;
    }

    private static final String IMAGE_VERSION = "postgres:16-alpine";

    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(IMAGE_VERSION).withReuse(true);

    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
    }

    @BeforeAll
    static void beforeAll() {
        postgres.start();
    }

    @AfterAll
    static void afterAll() {
        postgres.stop();
    }

    @Test
    @DisplayName("Should be get the integration test to GET /departments with success")
    public void testFindByAllDepartmentsByGetEndpoint() throws Exception {
        Department department0 = Department.builder()
            .id(UUID.fromString("dd6003fd-7df5-4c94-9d3e-217cf92cb387"))
            .name("HR")
            .build();
        Department department1 = Department.builder()
            .id(UUID.fromString("ff9ed973-b767-4254-897d-089b8cec393a"))
            .name("IT")
            .build();

        mockMvc.perform(get("/departments")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[0].id", is(department0.getId().toString())))
            .andExpect(jsonPath("$[0].name", is(department0.getName())))
            .andExpect(jsonPath("$[1].id", is(department1.getId().toString())))
            .andExpect(jsonPath("$[1].name", is(department1.getName())));

    }

}