package com.management.integration;

import com.management.configurations.i18n.I18nUtil;
import com.management.domain.entities.Department;
import com.management.domain.entities.UserAccount;
import java.util.Locale;
import java.util.UUID;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserAccountResourceIntegrationTest extends AbstractIntegrationTestConfiguration {

    private final MockMvc mockMvc;
    private final I18nUtil i18nUtil;

    @Autowired
    public UserAccountResourceIntegrationTest(
        MockMvc mockMvc,
        I18nUtil i18nUtil
    ) {
        this.mockMvc = mockMvc;
        this.i18nUtil = i18nUtil;
    }

    private static final String IMAGE_VERSION = "postgres:16-alpine";

    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(IMAGE_VERSION).withReuse(true);

    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
    }

    @BeforeAll
    static void beforeAll() {
        postgres.start();
    }

    @AfterAll
    static void afterAll() {
        postgres.stop();
    }

    @Test
    @SneakyThrows
    @DisplayName("Should be get the integration test to GET /user-accounts with success")
    public void testFindByAllUserAccountsByGETEndpointSuccess() {
        UserAccount userAccount0 = UserAccount.builder()
            .id(UUID.fromString("5edf4a0c-53ba-4d84-9312-be5cf4d96c2b"))
            .name("User Account 0")
            .email("useraccount0@email.com")
            .department(Department.builder()
                .id(UUID.fromString("dd6003fd-7df5-4c94-9d3e-217cf92cb387"))
                .name("HR")
                .build())
            .build();

        UserAccount userAccount1 = UserAccount.builder()
            .id(UUID.fromString("129843de-e77d-473d-91a3-28f9fb5a78ce"))
            .name("User Account 1")
            .email("useraccount1@email.com")
            .department(Department.builder()
                .id(UUID.fromString("ff9ed973-b767-4254-897d-089b8cec393a"))
                .name("IT")
                .build())
            .build();

        mockMvc.perform(get("/user-accounts")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[0].id", is(userAccount0.getId().toString())))
            .andExpect(jsonPath("$[0].name", is(userAccount0.getName())))
            .andExpect(jsonPath("$[0].email", is(userAccount0.getEmail())))
            .andExpect(jsonPath("$[0].department.id", is(userAccount0.getDepartment().getId().toString())))
            .andExpect(jsonPath("$[0].department.name", is(userAccount0.getDepartment().getName())))
            .andExpect(jsonPath("$[1].id", is(userAccount1.getId().toString())))
            .andExpect(jsonPath("$[1].name", is(userAccount1.getName())))
            .andExpect(jsonPath("$[1].email", is(userAccount1.getEmail())))
            .andExpect(jsonPath("$[1].department.id", is(userAccount1.getDepartment().getId().toString())))
            .andExpect(jsonPath("$[1].department.name", is(userAccount1.getDepartment().getName())));
    }

    @Test
    @SneakyThrows
    @DisplayName("Should be get the integration test to GET /user-accounts/uuid with success")
    public void testFindByIdUserAccountByGETEndpointSuccess() {
        UserAccount userAccount0 = UserAccount.builder()
            .id(UUID.fromString("5edf4a0c-53ba-4d84-9312-be5cf4d96c2b"))
            .name("User Account 0")
            .email("useraccount0@email.com")
            .department(Department.builder()
                .id(UUID.fromString("dd6003fd-7df5-4c94-9d3e-217cf92cb387"))
                .name("HR")
                .build())
            .build();

        mockMvc.perform(get("/user-accounts/" + userAccount0.getId())
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id", is(userAccount0.getId().toString())))
            .andExpect(jsonPath("$.name", is(userAccount0.getName())))
            .andExpect(jsonPath("$.email", is(userAccount0.getEmail())))
            .andExpect(jsonPath("$.department.id", is(userAccount0.getDepartment().getId().toString())))
            .andExpect(jsonPath("$.department.name", is(userAccount0.getDepartment().getName())));
    }

    @Test
    @SneakyThrows
    @DisplayName("Should be get the integration test to GET /user-accounts/{uuid} with EntityNotFoundException")
    public void testFindByIdUserAccountByGETEndpointEntityNotFoundException() {
        Locale locale = Locale.ENGLISH;
        String message = i18nUtil.getMessage(
            "exception.entity-not-found",
            locale,
            UserAccount.class.getSimpleName()
        );

        mockMvc.perform(get("/user-accounts/" + UUID.fromString("5edf4a0c-53ba-4d84-9312-be5cf4d96c2a"))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Accept-Language", locale))

            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_PROBLEM_JSON))
            .andExpect(jsonPath("$.detail", is(message)))
            .andExpect(jsonPath("$.title", is("Bad Request")));
    }

    @Test
    @SneakyThrows
    @DisplayName("Should be get the integration test to GET /user-accounts/123123 with BadRequestException")
    public void testFindByIdUserAccountByGETEndpointBadRequestException() {
        mockMvc.perform(get("/user-accounts/123123")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_PROBLEM_JSON))
            .andExpect(jsonPath("$.detail", is("Failed to convert 'id' with value: '123123'")))
            .andExpect(jsonPath("$.title", is("Bad Request")));
    }

}