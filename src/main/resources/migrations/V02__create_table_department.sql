CREATE TABLE department (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name varchar(255) NOT NULL,
    CONSTRAINT department_pk PRIMARY KEY (id),
    CONSTRAINT department_unique UNIQUE (name)
);