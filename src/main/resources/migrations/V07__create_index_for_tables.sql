CREATE INDEX idx_user_account_name ON user_account (name);
CREATE INDEX idx_department_id ON user_account (department_id);
CREATE INDEX idx_department_name ON department (name);