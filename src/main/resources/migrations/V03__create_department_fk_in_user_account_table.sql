ALTER TABLE user_account
    ADD department_id uuid;

ALTER TABLE user_account
    ADD CONSTRAINT department_fk
    FOREIGN KEY (department_id)
    REFERENCES department (id)
    ON DELETE SET NULL;