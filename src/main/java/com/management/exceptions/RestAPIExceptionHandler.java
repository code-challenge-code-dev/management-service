package com.management.exceptions;

import com.management.configurations.i18n.I18nUtil;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestAPIExceptionHandler extends ResponseEntityExceptionHandler {

    private final I18nUtil i18nUtil;

    @Autowired
    public RestAPIExceptionHandler(I18nUtil i18nUtil) {
        this.i18nUtil = i18nUtil;
    }

    public ProblemDetail getProblemDetail(
        HttpStatus status,
        String className,
        String message
    ) {
        ProblemDetail problemDetail = ProblemDetail.forStatus(status);
        problemDetail.setType(URI.create(className));
        problemDetail.setDetail(message);
        problemDetail.setProperty("offsetDateTime", Instant.now());

        return problemDetail;
    }

    @ExceptionHandler(value = { BusinessException.class })
    public ProblemDetail randleBusinessException(@NotNull BusinessException ex) {
        return getProblemDetail(
            HttpStatus.BAD_REQUEST,
            ex.getClass().getSimpleName(),
            ex.getMessage()
        );
    }

    @ExceptionHandler(value = { EntityNotFoundException.class })
    public ProblemDetail randleEntityNotFoundException(@NotNull EntityNotFoundException ex) {
        return getProblemDetail(
            HttpStatus.BAD_REQUEST,
            ex.getClass().getSimpleName(),
            ex.getMessage()
        );
    }

}