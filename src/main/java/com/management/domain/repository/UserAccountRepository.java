package com.management.domain.repository;

import com.management.domain.entities.UserAccount;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, UUID> {
    Page<UserAccount> findAll(Specification<UserAccount> spec, Pageable pageable);
}
