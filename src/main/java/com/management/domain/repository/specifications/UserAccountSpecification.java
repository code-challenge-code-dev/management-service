package com.management.domain.repository.specifications;

import com.management.domain.entities.UserAccount;
import org.springframework.data.jpa.domain.Specification;

public class UserAccountSpecification {

    public static Specification<UserAccount> nameContains(String name) {
        return (root, query, criteriaBuilder) ->
            criteriaBuilder.like(criteriaBuilder.lower(
                root.get("name")), "%" + name.toLowerCase() + "%");
    }

    public static Specification<UserAccount> departmentContains(String department) {
        return (root, query, criteriaBuilder) ->
            criteriaBuilder.like(criteriaBuilder.lower(
                root.join("department").get("name")), "%" + department.toLowerCase() + "%");
    }
}