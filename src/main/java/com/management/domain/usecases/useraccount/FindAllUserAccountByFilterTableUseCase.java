package com.management.domain.usecases.useraccount;

import com.management.domain.entities.UserAccount;
import org.springframework.data.domain.Page;

public interface FindAllUserAccountByFilterTableUseCase {
    Page<UserAccount> execute(int page, int size, String sortBy, String order, String searchText);
}
