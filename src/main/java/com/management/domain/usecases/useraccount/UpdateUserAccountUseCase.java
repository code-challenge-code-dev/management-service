package com.management.domain.usecases.useraccount;

import com.management.domain.entities.UserAccount;

public interface UpdateUserAccountUseCase {
    UserAccount execute(UserAccount userAccount);
}
