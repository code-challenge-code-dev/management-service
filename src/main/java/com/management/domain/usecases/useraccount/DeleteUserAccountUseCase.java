package com.management.domain.usecases.useraccount;

import java.util.UUID;

public interface DeleteUserAccountUseCase {
    void execute(UUID userAccountId);
}
