package com.management.domain.usecases.useraccount;

import com.management.domain.entities.UserAccount;

public interface CreateUserAccountUseCase {
    UserAccount execute(UserAccount userAccount);
}
