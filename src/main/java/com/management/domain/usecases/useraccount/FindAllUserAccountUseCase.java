package com.management.domain.usecases.useraccount;

import com.management.domain.entities.UserAccount;
import java.util.List;

public interface FindAllUserAccountUseCase {
    List<UserAccount> execute();
}
