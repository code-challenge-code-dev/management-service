package com.management.domain.usecases.department;

import com.management.domain.entities.Department;
import java.util.List;

public interface FindAllDepartmentUseCase {
    List<Department> execute();
}
