package com.management.domain.usecases.kafka;

import com.management.kafka.dtos.SendNotificationMessage;

public interface SendNotificationTopicProducerUseCase {
    void execute(SendNotificationMessage communicationMessage);
}
