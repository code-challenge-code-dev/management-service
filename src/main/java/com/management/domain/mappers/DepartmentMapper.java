package com.management.domain.mappers;

import com.management.domain.entities.Department;
import com.management.dtos.response.DepartmentResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring"
)
public interface DepartmentMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    DepartmentResponse mapDepartmentToDepartmentResponse(Department department);

}