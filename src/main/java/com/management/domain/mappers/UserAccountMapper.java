package com.management.domain.mappers;

import com.management.domain.entities.Department;
import com.management.domain.entities.UserAccount;
import com.management.dtos.request.CreateUserAccountRequest;
import com.management.dtos.response.UserAccountResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring",
    uses = {
        UserAccountMapper.class
    },
    imports = {
        Department.class
    }
)
public interface UserAccountMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "email", source = "email")
    @Mapping(
        target = "department",
        expression = """
            java(
                Department.builder()
                    .id(userAccount.getDepartment().getId())
                    .name(userAccount.getDepartment().getName())
                    .build()
            )
            """
    )
    UserAccountResponse mapUserAccountToUserAccountResponse(UserAccount userAccount);

    UserAccount mapUserAccountRequestToUserAccount(CreateUserAccountRequest createUserAccountRequest);

}
