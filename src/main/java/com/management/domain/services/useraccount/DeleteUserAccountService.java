package com.management.domain.services.useraccount;

import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.useraccount.DeleteUserAccountUseCase;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteUserAccountService implements DeleteUserAccountUseCase {

    private final UserAccountRepository userAccountRepository;

    @Override
    public void execute(UUID userAccountId) {
        userAccountRepository.deleteById(userAccountId);
    }

}