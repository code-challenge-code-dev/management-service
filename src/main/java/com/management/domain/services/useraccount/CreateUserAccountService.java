package com.management.domain.services.useraccount;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.kafka.SendNotificationTopicProducerUseCase;
import com.management.domain.usecases.useraccount.CreateUserAccountUseCase;
import com.management.kafka.dtos.SendNotificationMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateUserAccountService implements CreateUserAccountUseCase {

    private final UserAccountRepository userAccountRepository;
    private final SendNotificationTopicProducerUseCase sendNotificationTopicProducerUseCase;

    @Override
    public UserAccount execute(UserAccount userAccount) {
        var savedUserAccount = userAccountRepository.save(userAccount);

        sendNotificationTopicProducerUseCase.execute(
            new SendNotificationMessage(
                savedUserAccount.getId(),
                savedUserAccount.getEmail(),
                userAccount.getName(),
                "Created '" + savedUserAccount.getEmail() + "'"
            )
        );

        return savedUserAccount;
    }

}