package com.management.domain.services.useraccount;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.useraccount.FindAllUserAccountUseCase;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FindAllUserAccountService implements FindAllUserAccountUseCase {

    private final UserAccountRepository userAccountRepository;

    @Override
    public List<UserAccount> execute() {
        return userAccountRepository.findAll();
    }

}
