package com.management.domain.services.useraccount;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.repository.specifications.UserAccountSpecification;
import com.management.domain.usecases.useraccount.FindAllUserAccountByFilterTableUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FindAllUserAccountByFilterTableService implements FindAllUserAccountByFilterTableUseCase {

    private final UserAccountRepository userAccountRepository;

    @Override
    public Page<UserAccount> execute(int page, int size, String sortBy, String order, String searchText) {
        Sort sort = order.equalsIgnoreCase(
            Sort.Direction.ASC.name()
        ) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();

        Specification<UserAccount> spec = Specification.where(null);

        if (searchText != null && !searchText.isEmpty()) {
            spec = spec.and(UserAccountSpecification.nameContains(searchText));
        }


        Pageable pageable = PageRequest.of(page, size, sort);
        return userAccountRepository.findAll(spec, pageable);
    }

}