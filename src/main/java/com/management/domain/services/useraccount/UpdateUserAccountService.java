package com.management.domain.services.useraccount;

import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.useraccount.UpdateUserAccountUseCase;
import com.management.kafka.dtos.SendNotificationMessage;
import com.management.kafka.producer.NotificationToSendTopicProducerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UpdateUserAccountService implements UpdateUserAccountUseCase {

    private final UserAccountRepository userAccountRepository;
    private final NotificationToSendTopicProducerService sendCommunicationTopicProducerService;

    @Override
    public UserAccount execute(UserAccount userAccount) {
        var savedUserAccount = userAccountRepository.save(userAccount);

        sendCommunicationTopicProducerService.execute(
            new SendNotificationMessage(
                savedUserAccount.getId(),
                savedUserAccount.getEmail(),
                userAccount.getName(),
                "Updated '" + savedUserAccount.getEmail() + "'"
            )
        );

        return savedUserAccount;
    }
}