package com.management.domain.services.useraccount;

import com.management.configurations.i18n.I18nUtil;
import com.management.domain.entities.UserAccount;
import com.management.domain.repository.UserAccountRepository;
import com.management.domain.usecases.useraccount.FindUserAccountByIdUseCase;
import com.management.exceptions.EntityNotFoundException;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FindUserAccountByIdService implements FindUserAccountByIdUseCase {

    private final I18nUtil i18nUtil;
    private final UserAccountRepository userAccountRepository;
    
    @Override
    public UserAccount execute(UUID uuid) {
        return userAccountRepository.findById(uuid).orElseThrow(
            () -> new EntityNotFoundException(
                i18nUtil.getMessage(
                    "exception.entity-not-found",
                    UserAccount.class.getSimpleName()
                )
            )
        );
    }

}
