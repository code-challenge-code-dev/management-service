package com.management.domain.services.department;

import com.management.domain.entities.Department;
import com.management.domain.repository.DepartmentRepository;
import com.management.domain.usecases.department.FindAllDepartmentUseCase;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FindAllDepartmentService implements FindAllDepartmentUseCase {

    private final DepartmentRepository departmentRepository;

    @Override
    public List<Department> execute() {
        return departmentRepository.findAll();
    }

}
