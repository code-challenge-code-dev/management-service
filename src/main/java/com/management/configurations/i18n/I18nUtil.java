package com.management.configurations.i18n;

import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class I18nUtil {

    private final MessageSource messageSource;

    public String getMessage(String code, String... args){
        return messageSource.getMessage(code, args, Locale.getDefault());
    }

    public String getMessage(String code, Locale locale, String... args){
        return messageSource.getMessage(code, args, locale);
    }

}