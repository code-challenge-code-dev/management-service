package com.management.kafka.dtos;

import java.io.Serializable;
import java.util.UUID;

public record SendNotificationMessage(
    UUID accountId,
    String emailTo,
    String name,
    String payload
) implements Serializable {}