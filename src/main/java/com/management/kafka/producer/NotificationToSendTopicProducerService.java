package com.management.kafka.producer;

import com.management.domain.usecases.kafka.SendNotificationTopicProducerUseCase;
import com.management.kafka.dtos.SendNotificationMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationToSendTopicProducerService implements SendNotificationTopicProducerUseCase {

    @Value("${topic.name.notification-to-send}")
    private String topicName;

    private final KafkaTemplate<String, SendNotificationMessage> kafkaTemplate;

    public void execute(SendNotificationMessage message){
        log.info("Payload sent to Topic={}: {}", topicName, message);
        kafkaTemplate.send(topicName, message);
    }

}