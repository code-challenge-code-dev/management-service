package com.management.resources;

import com.management.domain.entities.UserAccount;
import com.management.domain.mappers.UserAccountMapper;
import com.management.domain.mappers.UserAccountMapperImpl;
import com.management.domain.usecases.useraccount.CreateUserAccountUseCase;
import com.management.domain.usecases.useraccount.DeleteUserAccountUseCase;
import com.management.domain.usecases.useraccount.FindAllUserAccountByFilterTableUseCase;
import com.management.domain.usecases.useraccount.FindAllUserAccountUseCase;
import com.management.domain.usecases.useraccount.FindUserAccountByIdUseCase;
import com.management.domain.usecases.useraccount.UpdateUserAccountUseCase;
import com.management.dtos.request.CreateUserAccountRequest;
import com.management.dtos.response.UserAccountFilterResponse;
import com.management.dtos.response.UserAccountResponse;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user-accounts")
@RequiredArgsConstructor
public class UserAccountResource {

    private final FindAllUserAccountUseCase findAllUserAccountUseCase;
    private final FindAllUserAccountByFilterTableUseCase findAllUserAccountByFilterTableUseCase;
    private final FindUserAccountByIdUseCase findUserAccountByIdUseCase;
    private final CreateUserAccountUseCase createUserAccountUseCase;
    private final UpdateUserAccountUseCase updateUserAccountUseCase;
    private final DeleteUserAccountUseCase deleteUserAccountUseCase;
    private final UserAccountMapper userAccountMapper = new UserAccountMapperImpl();

    @GetMapping
    public ResponseEntity<List<UserAccountResponse>> findByAll(){
        return ResponseEntity.ok().body(
            findAllUserAccountUseCase.execute().stream().map(userAccountMapper::mapUserAccountToUserAccountResponse
        ).toList());
    }

    @GetMapping("/filter")
    public ResponseEntity<UserAccountFilterResponse> findByAllByFilter(
        @RequestParam(required = false, defaultValue = "name") String sortBy,
        @RequestParam(required = false, defaultValue = "asc") String order,
        @RequestParam(required = false, defaultValue = "0") Integer page,
        @RequestParam(required = false, defaultValue = "5") Integer size,
        @RequestParam(required = false) String searchText
    ){
        var pageable = findAllUserAccountByFilterTableUseCase.execute(page, size, sortBy, order, searchText);

        var rest = pageable.stream().map(
            userAccountMapper::mapUserAccountToUserAccountResponse
        ).toList();

        return ResponseEntity.ok().body(
            UserAccountFilterResponse.builder()
                .items(rest)
                .totalCount(pageable.getTotalElements())
                .build()
        );
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserAccountResponse> findById(
        @PathVariable(value = "id") UUID id //,
//        @RequestHeader(name = "Accept-Language", required = false) Locale locale
    ){
        UserAccount userAccount = findUserAccountByIdUseCase.execute(id);
        return ResponseEntity.ok().body(userAccountMapper.mapUserAccountToUserAccountResponse(userAccount));
    }

    @PostMapping
    public ResponseEntity<UserAccountResponse> save(
        @RequestBody CreateUserAccountRequest createUserAccountRequest
    ){
        UserAccount userAccountSaved = createUserAccountUseCase.execute(
            userAccountMapper.mapUserAccountRequestToUserAccount(createUserAccountRequest)
        );
        return ResponseEntity.created(URI.create("/user-accounts/" + userAccountSaved.getId()))
            .body(userAccountMapper.mapUserAccountToUserAccountResponse(userAccountSaved));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(
        @PathVariable(value = "id") UUID id,
        @RequestBody CreateUserAccountRequest createUserAccountRequest
    ){
        updateUserAccountUseCase.execute(userAccountMapper.mapUserAccountRequestToUserAccount(createUserAccountRequest));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(
        @PathVariable(value = "id") UUID id
    ){
        deleteUserAccountUseCase.execute(id);
        return ResponseEntity.noContent().build();
    }

}