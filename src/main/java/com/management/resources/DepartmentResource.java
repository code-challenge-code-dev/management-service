package com.management.resources;

import com.management.domain.mappers.DepartmentMapper;
import com.management.domain.mappers.DepartmentMapperImpl;
import com.management.domain.usecases.department.FindAllDepartmentUseCase;
import com.management.dtos.response.DepartmentResponse;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/departments")
@RequiredArgsConstructor
public class DepartmentResource {

    private final FindAllDepartmentUseCase findAllDepartmentUseCase;
    private final DepartmentMapper departmentMapper = new DepartmentMapperImpl();

    @GetMapping
    public ResponseEntity<List<DepartmentResponse>> findByAll(){
        return ResponseEntity.ok().body(
            findAllDepartmentUseCase.execute().stream().map(
                departmentMapper::mapDepartmentToDepartmentResponse
            ).toList()
        );
    }

}