package com.management.dtos.request;

import com.management.domain.entities.Department;
import java.io.Serializable;
import java.util.UUID;
import lombok.Builder;

@Builder
public record CreateUserAccountRequest(
    UUID id,
    String name,
    String email,
    Department department
) implements Serializable {}