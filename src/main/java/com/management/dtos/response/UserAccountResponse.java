package com.management.dtos.response;

import com.management.domain.entities.Department;
import java.io.Serializable;
import java.util.UUID;
import lombok.Builder;

@Builder
public record UserAccountResponse(
    UUID id,
    String name,
    String email,
    Department department
) implements Serializable {}