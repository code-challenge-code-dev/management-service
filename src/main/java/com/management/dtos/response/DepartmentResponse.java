package com.management.dtos.response;

import java.io.Serializable;
import java.util.UUID;
import lombok.Builder;

@Builder
public record DepartmentResponse(
    UUID id,
    String name
) implements Serializable {}