package com.management.dtos.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Builder;

@Builder
public record UserAccountFilterResponse(
    List<UserAccountResponse> items,
    @JsonProperty(value = "total_count") long totalCount
) implements Serializable {}